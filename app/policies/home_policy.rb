# frozen_string_literal: true

class HomePolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    true
   end

  def create?
    user.present?
   end

  def update?
    return true if user.present? && user == home.user

    user.present? && user == home.user
   end

  def destroy?
    user.present? && user.admin?
   end

  private

  def home
    record
   end
end
