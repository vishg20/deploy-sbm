# frozen_string_literal: true

class ClientRegistrationPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    true
   end
   def show?
    true
  end
  def create?
   true
   end

  def update?
    user.present?
   end

  def destroy?
    user.present? && user.admin?
   end
   class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
     
      if user.admin?
        scope.all
      else
        scope.where(user_id: user)
     end
    end
    end

  private

  def client_registration
    record
   end
end
