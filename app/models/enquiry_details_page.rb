class EnquiryDetailsPage < ApplicationRecord
	belongs_to :enquiry
end
# == Schema Information
#
# Table name: enquiry_details_pages
#
#  id                            :bigint           not null, primary key
#  enquiry_number                :string(255)
#  enquiry_details               :string(255)
#  quotes_and_delivery_schedules :string(255)
#  tenant_id                     :string(255)
#  created_at                    :datetime         not null
#  updated_at                    :datetime         not null
#