class OperationsTeam < ApplicationRecord
	belongs_to :user
end
# == Schema Information
#
# Table name: operations_teams
#
#  id            :bigint           not null, primary key
#  name          :string(255)
#  mobile_number :string(255)
#  email_id      :string(255)
#  login_id      :string(255)
#  tenant_id     :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#