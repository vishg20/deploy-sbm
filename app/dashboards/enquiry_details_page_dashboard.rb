require "administrate/base_dashboard"

class EnquiryDetailsPageDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    enquiry_number: Field::String,
    enquiry_details: Field::String,
    quotes_and_delivery_schedules: Field::String,
    tenant_id: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :enquiry_number,
    :enquiry_details,
    :quotes_and_delivery_schedules,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :enquiry_number,
    :enquiry_details,
    :quotes_and_delivery_schedules,
    :tenant_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :enquiry_number,
    :enquiry_details,
    :quotes_and_delivery_schedules,
    :tenant_id,
  ].freeze

  # Overwrite this method to customize how enquiry details pages are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(enquiry_details_page)
  #   "EnquiryDetailsPage ##{enquiry_details_page.id}"
  # end
end
