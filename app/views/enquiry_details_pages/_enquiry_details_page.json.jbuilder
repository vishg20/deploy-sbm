json.extract! enquiry_details_page, :id, :enquiry_number, :enquiry_details, :quotes_and_delivery_schedules, :tenant_id, :created_at, :updated_at
json.url enquiry_details_page_url(enquiry_details_page, format: :json)
