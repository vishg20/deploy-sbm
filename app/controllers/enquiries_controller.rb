class EnquiriesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_enquiry, only: [:show, :update, :destroy]
  after_action :verify_authorized
  # GET /enquiries
  # GET /enquiries.json
  def index
    @enquiries = Enquiry.all
    authorize @enquiries
    @enquiries = policy_scope(Enquiry)
  end

  # GET /enquiries/1
  # GET /enquiries/1.json
  def show
    authorize @enquiry
  end
   # GET /enquiries/new
  def new

    @enquiry = Enquiry.new
    authorize @enquiry
  
  end
  # GET /enquiries/1/edit
  def edit
    @enquiry = Enquiry.find(params[:id])
    authorize @enquiry
  end
  # POST /enquiries
  # POST /enquiries.json
  def create
    @enquiry = Enquiry.new(enquiry_params.merge(user_id: current_user.id))
    authorize @enquiry

    if @enquiry.save
      render :show, status: :created, location: @enquiry
    else
      render json: @enquiry.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /enquiries/1
  # PATCH/PUT /enquiries/1.json
  def update
    authorize @enquiry
    if @enquiry.update(enquiry_params)
      render :show, status: :ok, location: @enquiry
    else
      render json: @enquiry.errors, status: :unprocessable_entity
    end
  end

  # DELETE /enquiries/1
  # DELETE /enquiries/1.json
  def destroy
    authorize @enquiry
    @enquiry.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_enquiry
      @enquiry = Enquiry.find(params[:id])
      authorize @enquiry
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def enquiry_params
      params.require(:enquiry).permit(:client_name, :enquiry_for, :color, :material, :gsm, :quantity, :sizes, :printing, :embroidery, :place_of_printing, :payment_terms, :client_target_price, :client_target_delivery_schedule, :owner_of_the_etask, :status_of_the_enquiry, :tenant_id, :client_id, :user_id)
    end
end
