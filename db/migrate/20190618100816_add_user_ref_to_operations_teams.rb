class AddUserRefToOperationsTeams < ActiveRecord::Migration[5.2]
  def change
    add_reference :operations_teams, :user, foreign_key: true
  end
end
