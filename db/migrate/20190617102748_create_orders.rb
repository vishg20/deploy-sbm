class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.string :customer_name
      t.string :enquiry_number
      t.string :client_name
      t.string :enquiry_for
      t.string :color
      t.string :material
      t.string :gsm
      t.integer :quantity
      t.string :sizes
      t.string :printing
      t.string :embroidery
      t.string :place_of_printing
      t.string :payment_terms
      t.string :client_target_price
      t.string :client_target_delivery_schedule
      t.string :offered_price
      t.string :offered_delivery_schedule
      t.string :status_of_the_enquiry
      t.string :supplier_name
      t.string :sample_readiness
      t.string :sample__delivery
      t.string :advance_received
      t.string :status_of_the_order
      t.string :owner_of_the_otask
      t.string :tenant_id

      t.timestamps
    end
  end
end
