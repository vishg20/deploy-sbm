class AddUserRefToEnquiryDetailsPages < ActiveRecord::Migration[5.2]
  def change
    add_reference :enquiry_details_pages, :user, foreign_key: true
  end
end
