# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_06_18_102921) do

  create_table "client_registrations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "company_name"
    t.string "buyer_name"
    t.string "phone_number"
    t.string "email_id"
    t.integer "mobile_number"
    t.string "purchasing_manager_name"
    t.string "address"
    t.string "nature_of_business"
    t.string "login_id"
    t.string "tenant_id"
    t.string "client_registration_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_client_registrations_on_user_id"
  end

  create_table "customisations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "type"
    t.string "place_of_printing"
    t.string "place_of_embroidery"
    t.string "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_customisations_on_user_id"
  end

  create_table "enquiries", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "client_name"
    t.string "enquiry_for"
    t.string "color"
    t.string "material"
    t.string "gsm"
    t.integer "quantity"
    t.string "sizes"
    t.string "printing"
    t.string "embroidery"
    t.string "place_of_printing"
    t.string "payment_terms"
    t.string "client_target_price"
    t.string "client_target_delivery_schedule"
    t.string "owner_of_the_etask"
    t.string "status_of_the_enquiry"
    t.string "tenant_id"
    t.string "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_enquiries_on_user_id"
  end

  create_table "enquiry_details_pages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "enquiry_number"
    t.string "enquiry_details"
    t.string "quotes_and_delivery_schedules"
    t.string "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_enquiry_details_pages_on_user_id"
  end

  create_table "homes", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "operations_teams", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "mobile_number"
    t.string "email_id"
    t.string "login_id"
    t.string "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_operations_teams_on_user_id"
  end

  create_table "orders", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "customer_name"
    t.string "enquiry_number"
    t.string "client_name"
    t.string "enquiry_for"
    t.string "color"
    t.string "material"
    t.string "gsm"
    t.integer "quantity"
    t.string "sizes"
    t.string "printing"
    t.string "embroidery"
    t.string "place_of_printing"
    t.string "payment_terms"
    t.string "client_target_price"
    t.string "client_target_delivery_schedule"
    t.string "offered_price"
    t.string "offered_delivery_schedule"
    t.string "status_of_the_enquiry"
    t.string "supplier_name"
    t.string "sample_readiness"
    t.string "sample__delivery"
    t.string "advance_received"
    t.string "status_of_the_order"
    t.string "owner_of_the_otask"
    t.string "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_orders_on_user_id"
  end

  create_table "supplier_registrations", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "company_name"
    t.string "supplier_name"
    t.string "phone_number"
    t.string "email_id"
    t.string "mobile_number"
    t.string "manager_name"
    t.string "address"
    t.string "nature_of_business"
    t.string "login_id"
    t.string "tenant_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.index ["user_id"], name: "index_supplier_registrations_on_user_id"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "client_registrations", "users"
  add_foreign_key "customisations", "users"
  add_foreign_key "enquiries", "users"
  add_foreign_key "enquiry_details_pages", "users"
  add_foreign_key "operations_teams", "users"
  add_foreign_key "orders", "users"
  add_foreign_key "supplier_registrations", "users"
end
