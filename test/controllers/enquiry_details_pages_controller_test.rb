require 'test_helper'

class EnquiryDetailsPagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @enquiry_details_page = enquiry_details_pages(:one)
  end

  test "should get index" do
    get enquiry_details_pages_url, as: :json
    assert_response :success
  end

  test "should create enquiry_details_page" do
    assert_difference('EnquiryDetailsPage.count') do
      post enquiry_details_pages_url, params: { enquiry_details_page: { enquiry_details: @enquiry_details_page.enquiry_details, enquiry_number: @enquiry_details_page.enquiry_number, quotes_and_delivery_schedules: @enquiry_details_page.quotes_and_delivery_schedules, tenant_id: @enquiry_details_page.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show enquiry_details_page" do
    get enquiry_details_page_url(@enquiry_details_page), as: :json
    assert_response :success
  end

  test "should update enquiry_details_page" do
    patch enquiry_details_page_url(@enquiry_details_page), params: { enquiry_details_page: { enquiry_details: @enquiry_details_page.enquiry_details, enquiry_number: @enquiry_details_page.enquiry_number, quotes_and_delivery_schedules: @enquiry_details_page.quotes_and_delivery_schedules, tenant_id: @enquiry_details_page.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy enquiry_details_page" do
    assert_difference('EnquiryDetailsPage.count', -1) do
      delete enquiry_details_page_url(@enquiry_details_page), as: :json
    end

    assert_response 204
  end
end
