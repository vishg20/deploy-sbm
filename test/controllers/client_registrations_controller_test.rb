require 'test_helper'

class ClientRegistrationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @client_registration = client_registrations(:one)
  end

  test "should get index" do
    get client_registrations_url, as: :json
    assert_response :success
  end

  test "should create client_registration" do
    assert_difference('ClientRegistration.count') do
      post client_registrations_url, params: { client_registration: { address: @client_registration.address, buyer_name: @client_registration.buyer_name, client_registration_id: @client_registration.client_registration_id, company_name: @client_registration.company_name, email_id: @client_registration.email_id, login_id: @client_registration.login_id, mobile_number: @client_registration.mobile_number, nature_of_business: @client_registration.nature_of_business, phone_number: @client_registration.phone_number, purchasing_manager_name: @client_registration.purchasing_manager_name, tenant_id: @client_registration.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show client_registration" do
    get client_registration_url(@client_registration), as: :json
    assert_response :success
  end

  test "should update client_registration" do
    patch client_registration_url(@client_registration), params: { client_registration: { address: @client_registration.address, buyer_name: @client_registration.buyer_name, client_registration_id: @client_registration.client_registration_id, company_name: @client_registration.company_name, email_id: @client_registration.email_id, login_id: @client_registration.login_id, mobile_number: @client_registration.mobile_number, nature_of_business: @client_registration.nature_of_business, phone_number: @client_registration.phone_number, purchasing_manager_name: @client_registration.purchasing_manager_name, tenant_id: @client_registration.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy client_registration" do
    assert_difference('ClientRegistration.count', -1) do
      delete client_registration_url(@client_registration), as: :json
    end

    assert_response 204
  end
end
