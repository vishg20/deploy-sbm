require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get orders_url, as: :json
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post orders_url, params: { order: { advance_received: @order.advance_received, client_name: @order.client_name, client_target_delivery_schedule: @order.client_target_delivery_schedule, client_target_price: @order.client_target_price, color: @order.color, customer_name: @order.customer_name, embroidery: @order.embroidery, enquiry_for: @order.enquiry_for, enquiry_number: @order.enquiry_number, gsm: @order.gsm, material: @order.material, offered_delivery_schedule: @order.offered_delivery_schedule, offered_price: @order.offered_price, owner_of_the_otask: @order.owner_of_the_otask, payment_terms: @order.payment_terms, place_of_printing: @order.place_of_printing, printing: @order.printing, quantity: @order.quantity, sample__delivery: @order.sample__delivery, sample_readiness: @order.sample_readiness, sizes: @order.sizes, status_of_the_enquiry: @order.status_of_the_enquiry, status_of_the_order: @order.status_of_the_order, supplier_name: @order.supplier_name, tenant_id: @order.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show order" do
    get order_url(@order), as: :json
    assert_response :success
  end

  test "should update order" do
    patch order_url(@order), params: { order: { advance_received: @order.advance_received, client_name: @order.client_name, client_target_delivery_schedule: @order.client_target_delivery_schedule, client_target_price: @order.client_target_price, color: @order.color, customer_name: @order.customer_name, embroidery: @order.embroidery, enquiry_for: @order.enquiry_for, enquiry_number: @order.enquiry_number, gsm: @order.gsm, material: @order.material, offered_delivery_schedule: @order.offered_delivery_schedule, offered_price: @order.offered_price, owner_of_the_otask: @order.owner_of_the_otask, payment_terms: @order.payment_terms, place_of_printing: @order.place_of_printing, printing: @order.printing, quantity: @order.quantity, sample__delivery: @order.sample__delivery, sample_readiness: @order.sample_readiness, sizes: @order.sizes, status_of_the_enquiry: @order.status_of_the_enquiry, status_of_the_order: @order.status_of_the_order, supplier_name: @order.supplier_name, tenant_id: @order.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete order_url(@order), as: :json
    end

    assert_response 204
  end
end
